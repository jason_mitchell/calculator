import "/js/lib/js.storage.min.js";

export var Name = "calc.utils"; //Module name
export var Description = "Utility commands"; //Module description

export default function(term, calc)
{    
    const store=Storages.initNamespaceStorage(Name).localStorage;

    term.clear = function() {$(".card").remove()}
    term.command("clear",term.clear)
    
    //Lists all of the local variables
    term.tempVariables = function()
    {
        term.newCard("Temporary Variables")
        for(var variable in calc.tempVars)
        {
            term.feedback(variable + ": " + calc.tempVars[variable]);
        }
        term.endCard()
    };

    //Lists all of the local variables
    term.permVariables = function()
    {
        term.newCard("Permanent Variables")
        for(var variable in calc.permVars)
        {
            term.feedback(variable + ": " + calc.permVars[variable]);
        }
        term.endCard()
    };

    term.command("clearPermVars", () => store.set("variables",{}))

    //Lists all of the persistent variables
    term.command("saved", function()
    {
        if(!store.isSet("variables"))
            store.set("variables", {});
        
        var vars = store.get("variables");
        for(var name in vars)
        {
            term.feedback(name + ": " + vars[name]);
        }
    });

    //Lists all of the registered constants
    term.constants = function() {
        term.newCard("CONSTANTS")
        for(var name in calc.constants)
        {
            var constant = calc.constants[name];
            term.feedback(name + "\r\t" + constant.description);
        }
        term.endCard()
    }
    term.command("constants", term.showConstants)
    
    //Lists all of the registered constants
    term.operators = function() {
        term.newCard("Operators")
        for(var name in calc.operators)
        {
            term.feedback(calc.operators[name]);
        }
        term.endCard()
    }

    //Lists all of the registered functions
    term.functions = function(namespace = "all") {
        term.newCard(namespace.toUpperCase())
        for(var name in calc.functions)
        {
            var func = calc.functions[name];
            if(namespace !== "all" && func.namespace !== namespace) continue;

            term.feedback(name +"(" + func.parameters.join(", ") + ") \r\t"+func.description);
            //term.feedback("[" + func.namespace + "] " + name +"(" + func.parameters.join(", ") + ")");
        }
        term.endCard()
    }
    term.command("functions", term.functions)

    //Saves a variable to persistent storage
    term.command("save", function(text)
    {
        if(!text)
        {
            term.feedback("Usage: save <name> [expression]");
            term.feedback("Sets a persistent variable");
            term.feedback("Setting a variable to null removes it from storage");
            return;
        }

        var split = text.split(" ");
        var name = split.shift().trim();
        var value = split.join(" ");
        if(split.length == 0) //Only name specified
            value = name;
        
        var calculated = calc.calculate(value);

        if(isNaN(calculated))
        {
            term.error("Value is invalid");
            return;
        }
        else
        {
            if(!store.isSet("variables"))
                store.set("variables", {});

            var vars = store.get("variables");
            vars[name] = calculated;
            calc.variables[name] = calculated; //Also update it in calculator variables
            store.set("variables", vars);
            term.success(`Saved variable ${name}`, vars);
        }
    });

    term.command("remove", function(text) 
    {
        var name = text.trim();
        var vars = store.get("variables");
        if(vars[name])
        {
            delete vars[name];
            store.set("variables", vars);
            term.success("Removed persistent variable " + name);
        }
        else
        {
            term.error("Persistent variable " + name + " does not exist");
        }
    })

    //Load variables
    let vars = store.get("variables");
    for(var v in vars)
        calc.variables[v] = vars[v];
}