export var Name = "calc.trigonometry";
export var Description = "Calculator trigonometry functions";

export default function(term, calc)
{
    /*===================
        Trigonometry
    ===================*/
    //Sine
    calc.function("trigonometry", "sin", ["x: number"], "Returns the sine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        let a = x * window.angularConversionFactor
        return Math.sin(a);
    });
    
    //Cosine
    calc.function("trigonometry", "cos", ["x: number"], "Returns the cosine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        let a = x * window.angularConversionFactor
        return Math.cos(a);
    });
    
    //Tangent
    calc.function("trigonometry", "tan", ["x: number"], "Returns the tangent of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        let a = x * window.angularConversionFactor
        return Math.tan(a);
    });
    
    //Inverse Sine
    calc.function("trigonometry", "asin", ["x: number"], "Returns the inverse sine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.asin(x) / window.angularConversionFactor;
    });
    
    //Cosine
    calc.function("trigonometry", "acos", ["x: number"], "Returns the inverse cosine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.acos(x) / window.angularConversionFactor;
    });
    
    //Tangent
    calc.function("trigonometry", "atan", ["x: number", "y: number"], "Returns the inverse tangent of a number, or the inverse tangent of an x and y coordinate.",function(x)
    {
        switch(arguments.length) {
            case 1: return Math.atan(x) / window.angularConversionFactor;
            case 2: return Math.atan2(arguments[0],arguments[1]);
            default: throw new TypeError("Incorrect number of arguments");
        }
        //if two inputs, use atan2
    });

    //Hyperbolic Sine
    calc.function("trigonometry", "sinh", ["x: number"], "Returns the hyperbolic sine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.sinh(x)
    });
    
    //Hyperbolic Cosine
    calc.function("trigonometry", "cosh", ["x: number"], "Returns the hyperbolic cosine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.cosh(x)
    });
    
    //Hyperbolic Tangent
    calc.function("trigonometry", "tanh", ["x: number"], "Returns the hyperbolic tangent of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.tanh(x);
    });
    
    //Inverse Hyperbolic Sine
    calc.function("trigonometry", "asinh", ["x: number"], "Returns the inverse hyperbolic sine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.asinh(x)
    });
    
    //Inverse Hyperbolic Cosine
    calc.function("trigonometry", "acosh", ["x: number"], "Returns the inverse hyperbolic cosine of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.acosh(x)
    });
    
    //Inverse Hyperbolic Tangent
    calc.function("trigonometry", "atanh", ["x: number"], "Returns the inverse hyperbolic tangent of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.atanh(x);
    });
};