export var Name = "calc.math";
export var Description = "Calculator math functions and constants";

export default function(term, calc)
{
    /*========================
        Numbers
    ========================*/
    function gcf_two_numbers(x, y) {
        x = Math.abs(x);
        y = Math.abs(y);
        while(y) {
            var t = y;
            y = x % y;
            x = t;
        }
        return x;
    }

    function lcm_two_numbers(x, y) {
        if ((typeof x !== 'number') || (typeof y !== 'number')) return false;
        return (!x || !y) ? 0 : Math.abs((x * y) / gcf_two_numbers(x, y));
    }

    calc.function("numbers", "abs", ["x: number"], "Returns the absolute value of a number", function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.abs(x);
    });
    
    calc.function("numbers", "round", ["x: number"], "Returns a decimal rounded to the nearest number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.round(x);
    });
    
    calc.function("numbers", "ipart", ["x: number"], "Returns the integer part of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.trunc(x);
    });
    
    calc.function("numbers", "fpart", ["x: number"], "Returns the fractional/decimal part of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return x%1;
    });

    calc.function("numbers", "floor", ["x: number"], "Returns the greatest integer less than or equal to its numeric argument",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.floor(x);
    });

    calc.function("numbers", "ceil", ["x: number"], "Returns the smallest integer greater than or equal to its numeric argument",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.ceil(x);
    });

    calc.function("numbers", "sign", ["x: number"], "Returns the sign of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.sign(x);
    });

    calc.function("numbers", "lcm", ["x: number", "y: number"], "Returns the least common multiple of two numbers",function(x, y)
    {
        if(arguments.length !== 2) throw new TypeError("Incorrect number of arguments")
        return lcm_two_numbers(x, y)
    });
    
    calc.function("numbers", "gcd", ["x: number", "y: number"], "Returns the greatest common denominator of two numbers.",function(x, y)
    {
        if(arguments.length !== 2) throw new TypeError("Incorrect number of arguments")
        return gcf_two_numbers(x, y)
    });
    
    /*============================
        Exponents & Logarithms
    ============================*/
    calc.function("powers", "exp", ["x: number"], "Returns e (the base of natural logarithms) raised to a power",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.exp(x);
    });
    
    calc.function("powers", "sqrt", ["x: number"], "Returns the square root of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.sqrt(x);
    });
    
    calc.function("powers", "cbrt", ["x: number"], "Returns the cube root of a number",function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.cbrt(x);
    });

    calc.function("powers", "hypot", ["x:number","y: number"], "Returns the square root of the sum of squares of its arguments.", function(...args) {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return Math.hypot(...args);
    });
    
    calc.function("powers", "root", ["x: number", "y: number"], "Returns the nth root of a number",function(x,y)
    {
        if(arguments.length !== 2) throw new TypeError("Incorrect number of arguments")
        return Math.pow(x, 1/y);;
    });

    calc.function("powers", "log", ["x: number", "y: number"], "Returns the natural logarithm of a number", function(x,y)
    {
        switch(arguments.length) {
            case 1: return Math.log10(x);
            case 2: return Math.log(x) / Math.log(y);
            default:throw new TypeError("Incorrect number of arguments")
        }
    });
    
    calc.function("powers", "ln", ["x: number"], "Returns the natural logarithm of a number, or a log of base constant e.", function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return Math.log(x);
    });

    //Math Constants
    calc.constant("math", "e", "The mathematical constant e. This is eulers number, the base natural of logarithms", Math.E);
    calc.constant("math", "pi", "The ratio of the circumference of a circle to its diameter", Math.PI);
    calc.constant("math", "phi", "The golden ratio", (1 + Math.sqrt(5))/2);
};