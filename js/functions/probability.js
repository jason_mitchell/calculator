export var Name = "calc.probability";
export var Description = "Calculator statistics";

export default function(term, calc)
{
    /*===================
        Probability
    ===================*/
    function factorial(x) {
        var factorial = 1;
        for (var i = 2; i <= x; i++) {factorial *= i;}
        return factorial;
    }

    calc.function("probability", "random", ["x: number", "y: number"], "Returns a random number between 0 and 1",function(x, y)
    {
        switch(arguments.length) {
            case 0: return Math.random(); //0-1
            case 1: return Math.random() * x; //0-x
            case 2: return (Math.random() * (y-x)) + x; //x-y
            default: throw new TypeError("Incorrect number of arguments");
        }
    });

    calc.function("probability", "factorial", ["x: integer"], "Returns a factorial of a number", function(x)
    {
        if(arguments.length !== 1) throw new TypeError("Incorrect number of arguments")
        return factorial(x);
    });

    calc.function("probability", "permutations", ["x: integer", "y: integer"], "Returns the number of permutations of two numbers, where both are nonnegative integers.",function(x, y)
    {
        if(arguments.length !== 2) throw new TypeError("Incorrect number of arguments")
        return factorial(x) / factorial(x-y)
    });
    
    calc.function("probability", "combinations", ["x: integer", "y: integer"], "Returns the number of combinations of two numbers, where both are nonnegative integers.",function(x, y)
    {
        if(arguments.length !== 2) throw new TypeError("Incorrect number of arguments")
        return factorial(x) / (factorial(y) * factorial(x-y))
    });
};