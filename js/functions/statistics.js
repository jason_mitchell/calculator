export var Name = "calc.statistics";
export var Description = "Calculator statistics";

export default function(term, calc)
{
    /*============================
        Statistics / Multiple Numbers
    ============================*/
    function max(...array) {
        console.log(array)
        return Math.max(...array);
    }

    function min(...array) {
        return Math.min(...array);
    }

    function range(...array) {
        return max(array) - min(array);
    }

    function midrange(...array) {
        return range(array) / 2;
    }

    function sum(...array) {
        var num = 0;
        for (var i = 0, l = array.length; i < l; i++) num += array[i];
        return num;
    }

    function mean(array) {
        return sum(array) / array.length;
    }

    function median(array) {
        array.sort(function(a, b) {
            return a - b;
        });
        var mid = array.length / 2;
        return mid % 1 ? array[mid - 0.5] : (array[mid - 1] + array[mid]) / 2;
    }

    function modes(array) {
        if (!array.length) return [];
        var modeMap = {},
            maxCount = 0,
            modes = [];

        array.forEach(function(val) {
            if (!modeMap[val]) modeMap[val] = 1;
            else modeMap[val]++;

            if (modeMap[val] > maxCount) {
                modes = [val];
                maxCount = modeMap[val];
            }
            else if (modeMap[val] === maxCount) {
                modes.push(val);
                maxCount = modeMap[val];
            }
        });
        return modes;
    }

    function variance(array) {
        var mean = mean(array);
        return mean(array.map(function(num) {
            return Math.pow(num - mean, 2);
        }));
    }

    function standardDeviation(array) {
        return Math.sqrt(variance(array));
    }

    calc.function("statistics", "max", ["x: number", "y: number"], "Returns the largest of a set of numbers",function(...args)
    {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return max(args);
    });

    calc.function("statistics", "min", ["x: number", "y: number"], "Returns the smallest of a set of numbers",function(...args)
    {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return min(args);
    });

    calc.function("statistics", "sum", ["x: integer", "y: integer"], "Returns the sum of the arguments.",function(...args)
    {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return sum(...args);
    });

    calc.function("statistics", "mean", ["x: integer", "y: integer"], "Returns the sum of the arguments.",function(...args)
    {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return mean(...args);
    });

    calc.function("statistics", "median", ["x: integer", "y: integer"], "Returns the sum of the arguments.",function(...args)
    {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return median(...args);
    });

    calc.function("statistics", "variance", ["x: integer", "y: integer"], "Returns the sum of the arguments.",function(...args)
    {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return variance(...args);
    });

    calc.function("statistics", "stdDev", ["x: integer", "y: integer"], "Returns the sum of the arguments.",function(...args)
    {
        if(arguments.length === 0) throw new TypeError("Incorrect number of arguments")
        return standardDeviation(...args);
    });
};
