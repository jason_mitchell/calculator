import "/js/lib/js.storage.min.js";

export var Name = "calc";
export var Description = "RPN Calculator";
export default function(term)
{
    var store=Storages.initNamespaceStorage('calculator').localStorage;

    var calc = {}
    calc.tempVars = {} //temporary variables, defined on the fly
    calc.permVars = {} //permanent variables, A-Z
    calc.constants = {} //mathematical constants
    calc.functions = {} //mathematical functions
    calc.lastResult = 0; //the last result produced by the evaluator

    if(store.isSet("lastResult")) {
        calc.lastResult = store.get("lastResult");
    }

    if(store.isSet("variables")) {
        calc.permVars = store.get("variables");
    }

    calc.operators = {
        "ans": { //previous answer
            precedence:16,
            left:false,
            evaluate: function()
            {
                return calc.lastResult;
            }
        },
        "^" : { //exponent
            precedence:15,
            left:false,
            evaluate: function(stack)
            {
                const b = stack.pop();
                const a = stack.pop();
                return a**b;
            }
        },
        "%" : { //modulus
            precedence:14,
            left:true,
            evaluate: function(stack)
            {
                const b = stack.pop();
                const a = stack.pop();
                return a % b;
            }
        },
        "*" : { //multiplication
            precedence:14,
            left:true,
            evaluate: function(stack)
            {
                const b = stack.pop();
                const a = stack.pop();
                return a * b;
            }
        },
        "/" : { //division
            precedence:14,
            left:true,
            evaluate: function(stack)
            {
                const b = stack.pop();
                const a = stack.pop();
                return a / b;
            }
        },
        "!" : { //factorial
            precedence:14,
            left:false,
            evaluate: function(stack)
            {
                var factorial = 1;
                const a = stack.pop()
                for (var i = 2; i <= a; i++) {factorial *= i;}
                return factorial;
            }
        },
        "+" : { //addition
            precedence:13,
            left:true,
            evaluate: function(stack)
            {
                const b = stack.pop();
                const a = stack.pop();
                return a + b;
            }
        },
        "-" : { //binary subtractor
            precedence:13,
            left:true,
            evaluate: function(stack)
            {
                const b = stack.pop();
                const a = stack.pop();
                return a - b;
            }
        },
        "_" : { //unary negator
            precedence:13,
            left:false,
            evaluate: function(stack)
            {
                const a = stack.pop();
                return -a;
            }
        },
        ">" : { //greater than
            precedence:11,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b > a) ? 1 : 0;
            }
        },
        "<" : { //less than
            precedence:11,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b < a) ? 1 : 0;
            }
        },
        "<=" : { //less than
            precedence:11,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b <= a) ? 1 : 0;
            }
        },
        ">=" : { //less than
            precedence:11,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b >= a) ? 1 : 0;
            }
        },
        "==" : { //equal to
            precedence:11,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b >= a) ? 1 : 0;
            }
        },
        "!=" : { //not equal to
            precedence:11,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b !== a) ? 1 : 0;
            }
        },
        "and" : { //not equal to
            precedence:9,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b & a) ? 1 : 0;
            }
        },
        "xor" : { //not equal to
            precedence:8,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b ^ a) ? 1 : 0;
            }
        },
        "or" : { //not equal to
            precedence:7,
            left:true,
            evaluate: function(stack)
            {
                const a = stack.pop();
                const b = stack.pop();
                return (b | a) ? 1 : 0;
            }
        },
        "=" : { //temporary assignment
            precedence:3,
            left:false,
            evaluate: function(stack)
            {
                const value = stack.pop();
                const name = stack.pop();
                calc.variable(name, value, false)
                return value;
            }
        },
        ":=" : { //permanent assignment
            precedence:3,
            left:false,
            evaluate: function(stack)
            {
                const value = stack.pop();
                const name = stack.pop();
                calc.variable(name, value, true)
                return value;
            }
        },
        "(" : {
            special:true //Special just means it will be ignored by infixToPostfix, as it has its own handling of certain characters
        },
        ")" : {
            special:true
        },
        "," : {
            special:false,
            left:true,
            evaluate: function(stack)
            {
                return ","
            }

        },
    }

    /**
     * Checks if a token is an operator
     * @param {string} token 
     * @param {boolean} includeSpecial 
     */
    function isOperator(token, includeSpecial=false)
    {
        var op = calc.operators[token];
        if(op == null)
            return false;
        
        if(op.special && !includeSpecial)
            return false;
        return true;
    }

    /**
     * Checks if a token is a number
     * @param {string} token 
     */
    function isNumeric(token)
    {
        return !isNaN(parseFloat(token))
    }
    
    /**
     * Returns the end/top of a array
     */
    function top(array)
    {
        return array[array.length-1]
    }

    function toInfix(expr) 
    {
        expr = expr.replace(/\s/g, "")
        var tokens = [];
        var debug = [];
        var string = "";

        debug.push("Tokenizer: (input) "+expr)

        const acceptedTokens = Object.keys(calc.operators).sort((a,b) => a.length - b.length);
        const acceptedFuncs = Object.keys(calc.functions).sort((a,b) => a.length - b.length);

        for(var i=0; i < expr.length; i++) 
        {
            let token; 

            acceptedTokens.forEach((op) =>  //check if a token starts here
            {
                if(expr.substring(i, i + op.length) === op)
                {
                    token = op;
                    debug.push("("+i+") Operator: "+token)
                    return;
                }
            });
            
            acceptedFuncs.forEach((op) =>  //check if a token starts here
            {
                if(expr.substring(i, i + op.length) === op)
                {
                    token = op;
                    debug.push("("+i+") Operator: "+token)
                    return;
                }
            });

            //How it shud be
            if(token) {
                //Non-operator string has ended, push it to tokens
                if(string !== "") {
                    tokens.push(string);
                    debug.push("("+i+") Number: "+token)
                    string = "";
                }
                let lastToken = top(tokens) //get the last token

                if(token === "-" && (isOperator(lastToken,true) && lastToken !== ")" && lastToken !=="ans" || i===0))
                    token = "_";

                tokens.push(token);
                debug.push("("+i+") Tokens: "+tokens)
                i += token.length-1; //Extra increment
            }
            else {
                string += expr[i];
            }
        }      
        if(string) { //push the remaining token
            tokens.push(string)
        }
        
        term.debug("Tokenizer: ("+tokens+")", debug)
        return tokens;
    }

    function executeFunctions(infix)
    {
        for(var index = 0; index < infix.length; index++)
        {
            const func = calc.functions[infix[index]];
            if(func) //Start of function!
            {
                let start = index;
                let end = infix.length;

                if(infix[index+=1] != "(")
                    throw new SyntaxError("Expected (");
                
                let args = [];
                var arg = []; //Current argument in infix form

                function pushArg()
                {
                    if(arg && arg.length > 0)
                    {
                        //Execute any functions in this argument and calculate it
                        executeFunctions(arg); 
                        let postFix = infixToPostfix(arg);
                        let argValue = evaluatePostFix(postFix);
                        args.push(argValue);
                        arg = [];
                    }
                }

                //max(1,2,3,min(4,5,6))
                let depth = 0;
                for(index; index < infix.length; index++) //Start reading the arguments
                {
                    let token = infix[index];
                    if(token === ",")
                    {
                        if(depth == 1) //If depth is 1(the root of the function) push the current argument to args
                            pushArg();
                        else //Part of a nested function, push to arg
                            arg.push(token);
                    }
                    else if(token === "(")
                    {
                        if(depth > 0) //Isn't the function start, push it to arg
                            arg.push(token);
                        depth++;
                    }
                    else if(token === ")")
                    {
                        depth--;
                        if(depth === 0) //Function has ended
                        {                            
                            end = index; //Record the end so we can chop the function out of the infix array and insert its result
                            pushArg();
                            break;
                        }
                        else //Part of a nested function, push it to arg
                        {
                            arg.push(token);
                        }
                    }
                    else
                    {
                        arg.push(token);
                    }
                }
                pushArg(); //Push remainder

                let result = func.execute(...args);
                term.debug(`Executed ${func.name}(${args.join(",")}) = ${result}`);

                //Chop out the function and replace it with the result
                infix.splice(start, (end-start)+1, result);
                index = start+1; //Return index back to the function start
            }
        }
    }

    //Converts tokenized infix array to postfix
    function infixToPostfix(tokens)
    {
        var stack = [] //operators
        var queue = [] //output
        var debug = [] //debug
        var previousToken = "";

        const variables = Object.assign(calc.tempVars, calc.permVars);

        function popStackToQueue() {
            queue.push(stack.pop());
            debug.push("("+i+") Stack: "+stack)
            debug.push("("+i+") Queue: "+queue)
        }

        debug.push("(input) Infix: "+tokens)
        for(var i=0; i<tokens.length; i++)
        {
            const token = tokens[i];
            const op = calc.operators[token];

            //debug.push("("+i+") "+token)
            
            if(isNumeric(token))
            {
                queue.push(token);
                debug.push("("+i+") Queue: "+queue)
            }
            else if(calc.constants[token] !== undefined)
            {
                queue.push(calc.constants[token].value)
            }
            else if(variables[token] !== undefined)
            {
                const assOps = ["=", "+=", "-=", "*=", "/=", "++", "--"]
                if(!assOps.includes(tokens[i+1]))
                {
                    queue.push(variables[token])
                }
            }
            else if(op !== undefined && !op.special)
            {
                while(stack.length > 0 && top(stack) !== "(")
                {
                    const topOp = calc.operators[top(stack)];
                    if(topOp !== undefined)
                    {
                        if(topOp.precedence > op.precedence)
                        {
                            popStackToQueue();
                            continue;
                        }
                        
                        if(topOp.precedence === op.precedence && topOp.left)
                        {
                            popStackToQueue();
                            continue;
                        }
                    }
                    
                    if(stack.length===0) throw new SyntaxError("Mismatch in parenthesis!");

                    break;
                }

                stack.push(token);
                debug.push("("+i+") Stack: "+stack)
            }
            else if(token === "(")
            {
                stack.push(token);
                debug.push("("+i+") Stack: "+stack)
            }
            else if(token === ")")
            {
                while(top(stack) !== "(") {
                    popStackToQueue();
                    if(stack.length===0) throw new SyntaxError("Mismatch in parenthesis!");
                }
                stack.pop();
                debug.push("("+i+") Stack: "+stack)
            }
            else if(token instanceof String && !token.match(/[A-Za-z0-9]/g))
            {
                throw new TypeError("Invalid token: " + token);
            }
            else
            {
                queue.push(token);
                debug.push("("+i+") Queue: "+queue)
                //stack.push(token);
            }
            previousToken = token;
        };

        while(stack.length)
        {
            let token = stack.pop()
            if (token === "(" || token === ")") throw new SyntaxError("Parentheses mismatched");
            queue.push(token)
            debug.push("("+i+") Queue: "+queue)
        }

        term.debug("SYA: ("+queue+")", debug)

        return queue;
    }

    //Does the maths!
    function evaluatePostFix(postfix)
    {
        /* RPN Algorithm:
        for each token in the postfix expression
        {
            if token is an operator
            {
                operand_2 = pop from the stack
                operand_1 = pop from the stack
                result = evaluate token with operand_1 and operand_2
                push result back onto the stack
            }
            else if token is an operand
            {
                push token onto the stack
            }
        }
        result = pop from the stack
        */

        //Run the RPN algorithm
        var stack = [];
        var debug = [];
        
        debug.push("(input) Postfix: "+postfix)
        for(var i=0; i<postfix.length; i++) {
            const token = postfix[i]

            var op = calc.operators[token];
            if(op != undefined)
            {
                let result = op.evaluate(stack);
                stack.push(result);   
                debug.push("("+i+") Stack: "+stack) 
            }
            else if(isNumeric(token))
            {
                stack.push(parseFloat(token));
                debug.push("("+i+") Stack: "+stack)
            }
            else //token is probably part of a variable assignment
            {
                stack.push(token);
                debug.push("("+i+") Stack: "+stack)
            }
            //else
            //{
            //    throw new Error(`Error in RPN function at token ${token}`)
            //}
        };
        
        const result = stack.pop()

        if(isNumeric(result) === false) {throw new TypeError("Result is not a number!")}
        
        term.debug("RPN: ("+result+")", debug)

        calc.lastResult = result;      
        store.set("lastResult",result)

        return result;
    }

    /**
     * Registers a new calculator function
     * @param {string} namespace 
     * @param {string} name 
     * @param {string[]} parameters 
     * @param {string} description 
     * @param {function} func 
     */
    calc.function = function(namespace, name, parameters, description, func)
    {
        var lower = name.toLowerCase();
        calc.functions[lower] = {
            name:lower,
            execute:func,
            parameters,
            description:description,
            namespace:namespace
        };
        term.debug("Added function " + name);
    }

    /**
     * Registers a new calculator constant
     * @param {string} namespace 
     * @param {string} name 
     * @param {string} description 
     * @param {number} value 
     */
    calc.constant = function(namespace, name, description, value)
    {
        calc.constants[name.toLowerCase()] = {
            namespace:namespace,
            description:description,
            value:value
        }
        term.debug("Added constant " + name);
    }


    /**
     * Registers a new calculator constant
     * @param {string} namespace 
     * @param {string} name 
     * @param {number} value 
     */
    calc.variable = function(name, value, isPermanent)
    {
        if(calc.constants[name] || calc.functions[name])
        {
            throw "You may not overwrite a constant or function with the same name."
        }
        else if(!/^[a-zA-Z]+$/.test(name)) 
        {
            throw "Variable names may only contain letters."
        }
        else
        {
            if(isPermanent)
            {
                calc.permVars[name] = value
                term.debug(`Set permanent variable ${name} to ${value}`, calc.permVars);
                store.set("variables", calc.permVars);
            }
            else
            {
                calc.tempVars[name] = value
                term.debug(`Set temporary variable ${name} to ${value}`, calc.tempVars);
            }

        }
    }

    /**
     * Calculates expressionString
     * @param {string} expressionString 
     */
    calc.calculate = function(expressionString)
    {
        term.debug("Calculating " + expressionString);

        //Tokenize the expression, this step will also replace variable tokens with their actual values
        var tokens = toInfix(expressionString);
        executeFunctions(tokens);

        //Convert the tokenized expression into a tokenized post-fix expression
        var postfix = infixToPostfix(tokens);
        
        //Crunch the numbers, call the math functions, etc.
        var result = evaluatePostFix(postfix);
        return result;
    }

    return calc;
}