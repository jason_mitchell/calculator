export function Units(term) {
    var store=Storages.initNamespaceStorage('calculator').localStorage;
    var unit = {}

    var UserOptions = window.UserOptions = {
        //Unit Format Settings
        get UnitFormat() {
            if(!store.isSet("unitFormat")) {
                store.set("unitFormat", "unitless");
            }
            return store.get("unitFormat");
        },
        set UnitFormat(value) {
            let curValue = UserOptions.UnitFormat;

            $("#btn-"+curValue).removeClass("active");
            $("#btn-"+value).addClass("active");
            store.set("unitFormat", value);

            term.feedback("Unit Format set to "+value+".")
        },

        //Angle Format Settings
        get AngleFormat() {
            if(!store.isSet("angleFormat")) {
                store.set("angleFormat", "radians");
            }
            return store.get("angleFormat");
        },
        set AngleFormat(value) {
            let curValue = UserOptions.AngleFormat;
            
            $("#btn-"+curValue).removeClass("active");
            $("#btn-"+value).addClass("active");
            store.set("angleFormat", value);
            
            window.angularConversionFactor = value ==="degrees" ? Math.PI / 180 : 1
            term.feedback("Angle Mode set to "+value+".")
        },

        //Unit Precision Settings
        get Precision() {
            if(!store.isSet("unitPrecision")) {
                store.set("unitPrecision", 2);
            }
            return store.get("unitPrecision");
        },
        set Precision(value) {
            let curValue = UserOptions.Precision;
            
            $("#btn-precision-"+curValue).removeClass("active");
            $("#btn-precision-"+value).addClass("active");
            store.set("unitPrecision", value);

            term.feedback("Unit Precision set to "+(value > 0 ? value : "float")+".")
        },

        //Theme Settings
        get Theme() {
            if(!store.isSet("theme")) {
                store.set("theme", "light");
            }
            return store.get("theme");
        },
        set Theme(value) {
            let curValue = UserOptions.Theme;
            
            $("#btn-theme-"+curValue).removeClass("active");
            $("#btn-theme-"+value).addClass("active");
            $("#page-theme").attr("href","./css/themes/"+value+".css")
            store.set("theme", value);
        }
    }
    return unit;
}