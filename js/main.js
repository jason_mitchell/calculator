import {Terminal} from "./terminal.js"
import {Units} from "./unitHandler.js"

var term = Terminal();
var calc;
term.loadModule("./calc.js").then((_calc) => {
    calc = _calc;
    term.loadModule("./commands/calc.js", calc); //Load calculator commands
    term.loadModule("./functions/math.js", calc)
    term.loadModule("./functions/statistics.js", calc)
    term.loadModule("./functions/trigonometry.js", calc)

    term.debug(calc);
});
Units(term);

//Initialize active buttons
UserOptions.UnitFormat = UserOptions.UnitFormat;
UserOptions.AngleFormat = UserOptions.AngleFormat;
UserOptions.Precision = UserOptions.Precision;
UserOptions.Theme = UserOptions.Theme;

//find a proper place for this stuff, bobo
Number.prototype.format = function() {
    switch(UserOptions.UnitFormat) {
        case "decimal": 
        return this;

        case "scientific": 
        return this.toExponential();

        case "fractional" : 
        return (new Fraction(this)).toFraction(true);
        
        case "fractional" : 
        return (new Fraction(this)).toFraction(true);

        default: 
        return this;
    }
}

/* =========================
    Global Setup
==========================*/

//Hello World
term.success("JMitchStuff Calculator version 1.5")

//Default Command is to calculate
term.defaultCommand = function(string) {
    term.feedback(string)
    try
    {
        term.success(calc.calculate(string).format());
    }
    catch(e)
    {
        if(term.getLogLevelString() == "debug" && e.stack)
            term.error(e, e.stack);
        else
            term.error(e);
    }
}

/* =========================
    Click event settings
==========================*/
//Console buttons
$("#btn-clear").click(function() {term.clear()})

//unit button click events
$("#btn-degrees").click( function() {UserOptions.AngleFormat = "degrees"; })
$("#btn-radians").click( function() {UserOptions.AngleFormat = "radians"; })

//unit button click events
$("#btn-decimal").click( function() { UserOptions.UnitFormat = "decimal" })
$("#btn-scientific").click( function() { UserOptions.UnitFormat = "scientific" })
$("#btn-fractional").click( function() { UserOptions.UnitFormat = "fractional" })

//command click events
$("#btn-catalog-operators").click(function() {term.operators()})
$("#btn-catalog-constants").click(function() {term.constants()})
$("#btn-catalog-tempvars").click(function() {term.tempVariables()})
$("#btn-catalog-permvars").click(function() {term.permVariables()})

//catalog click events
$("#btn-catalog-all").click(function() {term.functions()})
$("#btn-catalog-identity").click(function() {term.functions("numbers")})
$("#btn-catalog-powers").click(function() {term.functions("powers")})
$("#btn-catalog-trigonometry").click(function() {term.functions("trigonometry")})
$("#btn-catalog-probability").click(function() {term.functions("probability")})
$("#btn-catalog-statistics").click(function() {term.functions("statistics")})
    
//log level button click events
$("#btn-lvl-success").click( function() { term.setLogLevel("success") })
$("#btn-lvl-error").click( function() { term.setLogLevel("error") })
$("#btn-lvl-info").click( function() { term.setLogLevel("info") })
$("#btn-lvl-warning").click( function() { term.setLogLevel("warning") })
$("#btn-lvl-debug").click( function() { term.setLogLevel("debug") })

//theme button click events
$("#btn-theme-light").click( function() { UserOptions.Theme = "light" })
$("#btn-theme-dark").click( function() { UserOptions.Theme = "dark" })
$("#btn-theme-ti83").click( function() { UserOptions.Theme = "ti83" })