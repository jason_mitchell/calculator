export function Terminal() {
    var store=Storages.initNamespaceStorage('calculator').localStorage;
    var term = {} //Global term object, so we can access term from anywhere

    ///////////////////
    //Local variables//
    ///////////////////
    var logLevels = ["feedback","success", "error", "info", "warning", "debug"];
    var logLevel = 4;
    
    var outputDiv = $("#Output");
    var inputDiv = $("#input-line");
    var currentCard;

    var modules = {}
    var hotkeys = {}
    var commands = {}

    var history = [];
    var historyIndex = 0;
    var historyLimit = 20;
    var ansInserted = false;

    if(store.isSet("history")) {
        history = store.get("history");
    }

    /**
     * Appends a line to the terminal with log themes
     * Uses adds term-<level> style for theming
     * @param {string} msg 
     * @param {object} data 
     * @param {string} level 
     */
    function log(msg, data, level = "debug")
    {        
        if(logLevels.indexOf(level) <= logLevel) {
            if(!currentCard)
                term.newCard();

            let lineclass = ""
            let textclass = ""
            if(level=="feedback") {
                lineclass = "prompt input"
            }
            else
            {
                lineclass = "prompt output response "+level

                if(data)
                {
                    textclass = "type term-data-header"
                }
                else
                {
                    textclass = "type "+(typeof msg)
                }
            }
            
            let newLine = $( "<div></div>").addClass("Line")
            let linedata = $( "<div></div>").addClass(lineclass)
            let linetext = $( "<div>"+msg+"</div>").addClass(textclass)

            currentCard.append(newLine.append(linedata.append(linetext)))

            if(data)
            {
                let dataText = data;
                if(dataText instanceof Array || dataText instanceof Object)
                    dataText = JSON.stringify(dataText, null, 4);
                    
                if(dataText instanceof Function)
                    dataText = `${dataText}`;

                let expandIcon = $("<div style='font-size:0.6em; margin-top:auto; margin-bottom:auto;'></div>") //Too lazy to make css class, they get removed every time anyway >:o
                    .text("{...}")
                    .appendTo(linedata);

                let dataDiv = $("<div></div>")
                    .addClass("collapse")
                    .appendTo(newLine);

                $("<div></div>")
                    .addClass("term-data-body")
                    .text(dataText)
                    .appendTo(dataDiv);

                linetext.click(() =>
                {
                    dataDiv.toggleClass("collapse");
                    expandIcon.text(dataDiv.hasClass("collapse") ? "{...}" : "");
                });
            }
        }
    }

    term.html = function(html)
    {
        if(!currentCard)
            term.newCard();        
        
        let newLine = $( "<div></div>").addClass("Line").appendTo(currentCard);
        let div = $("<div></div>")
            .addClass("term-html")
            .appendTo(newLine);

        if(html.jquery)
        {
            console.log("Logging html element");
            div.append(html);
        }
        else
        {
            console.log("Logging html string");
            div.html(html);
        }
    }

    term.newCard = function(header)
    {
        if(currentCard && currentCard.children().length === 0)
            currentCard.remove();

        var card = $("<div></div>").addClass("card").addClass("m-2").prependTo(outputDiv)

        if(header)
            $("<div></div>").addClass("card-header").text(header).appendTo(card);

        currentCard = $("<div></div>").addClass("card-body").appendTo(card);
    }

    term.endCard = function()
    {
        currentCard = null;
    }

    term.feedback  = (msg, data) => log(msg, data,"feedback");
    term.success   = (msg, data) => log(msg, data, "success");
    term.error     = (msg, data) => log(msg, data, "error");
    term.info      = (msg, data) => log(msg, data, "info");
    term.warning   = (msg, data) => log(msg, data, "warning");
    term.debug     = (msg, data) => log(msg, data, "debug");

    /**
     * Registers a new hotkey
     * @param {int} keyCode 
     * @param {function} callback 
     */
    term.hotkey = function(keyCode, callback) {
        hotkeys[keyCode] = callback;
    }

    /**
     * Registers a new command
     * @param {string} name
     * @param {function} callback
     */
    term.command = function(name, callback) {
        commands[name] = callback;
        term.debug(`Registered new command ${name}`);
    }

    
    /**
     * Clears the entry historys
     */
    term.clearEntries = function() {
        history = [];
        store.set("history",[])

        term.newCard()
        term.success("Entry history erased.")
        term.endCard()
    }


    /**
     * Sets the minimum log level.
     * Accepts a string, see logLevels for list of levels
     * @param {string,number} level 
     */
    term.setLogLevel = function(level)
    {
        if(!isNaN(level)) //level is already a number
        {
            if(level < 0)
            {
                term.error("Log level must be greater than or equal to 0");
                return false;
            }

            if(level > logLevels.length-1)
            {
                term.error("Log level must be less than or equal to " + logLevels.length-1);
                return false;
            }

            $("#btn-lvl-"+term.getLogLevelString()).removeClass("active")

            logLevel = level;
            store.set("logLevel", logLevel);

            $("#btn-lvl-"+term.getLogLevelString()).addClass("active")

            term.success("Log level set to "+logLevel);

            return true;
        }
        else //Get the index of the specified level
        {
            var index = logLevels.indexOf(level);
            if(index < 0)
            {
                term.error("Invalid log level");
                return false;
            }
            
            return term.setLogLevel(index); //Now that we've got the actual index, let's try that again!
        }
    }

    term.getLogLevelString = () => logLevels[logLevel];
    term.getLogLevels = () => logLevels;
    term.getLogLevel = function()
    {
        if(!store.isSet("logLevel"))
            term.setLogLevel(logLevel);
        return store.get("logLevel");
    }

    /**
     * Executes a command
     * @param {string} commandString 
     */
    term.execute = function(commandString) {
        //term.endCard();
        term.newCard();

        //Get command and arguments
        if(commandString.charAt(0) === ":") {
            let string = commandString.substring(1).trim();

            var split = string.split(" ");
            var cmd = commands[split.shift().trim()];
            var text = split.join(" ").trim();
    
            if(cmd != null) { //If command is valid, use it
                if(text == "")
                    cmd();
                else
                    cmd(text);
            }
            else if(term.defaultCommand) { //Otherwise, forward command term.defaultCommand
                term.defaultCommand(commandString);
            }
        }
        else if(term.defaultCommand) { //Otherwise, forward command term.defaultCommand
            term.defaultCommand(commandString);
        }

        history.unshift(commandString)
        
        while(history.length > historyLimit) {history.pop()}
        
        store.set("history",history)
        term.endCard();
    }

    /**
     * Loads a module
     * @param {*} path Path to the module, relative to the calling module
     * @param {*} args Any additional arguments to pass to the module
     */
    term.loadModule = async function (path, ...args)
    {
        var mod = await import(path);
        modules[path] = mod;
        return mod.default(this, ...args);
    }

    //
    //
    /**
     * Hotkey input
     * This is where backspace, enter, etc. happen
     * Register new hotkeys with term.hotkey(keycode, function(){});
     */
    inputDiv.keydown(function(event) {
        var char = event.which || event.keyCode;
        if(hotkeys[char]) { hotkeys[char]() };
    });

    inputDiv.keyup(function(event) {
        if(ansInserted === false) {
            const input = inputDiv.val()
    
            if("^*/+-".indexOf(input[0]) !== -1)
            {
                inputDiv.val("ans" + input)
                ansInserted = true;
            }
        }
    })
    
    //Enter hotkey
    term.hotkey(13, function() {
        if(inputDiv.val()==='') return
        
        historyIndex = 0;
        ansInserted = false;

        var text = inputDiv.val();
        term.execute(text)
        inputDiv.val('');
    });

    function loadHistory(index) {
        historyIndex = index;
        inputDiv.val(history[historyIndex-1]);
    }

    //Arrow Up hotkey
    term.hotkey(38, function() {
        if(historyIndex < history.length) {
            loadHistory(historyIndex+1);
        }
    });

    //Arrow Down hotkey
    term.hotkey(40, function() {
        if(historyIndex > 0) {
            loadHistory(historyIndex-1);
        }
    });

    /**
     * Initialize the term
     */
    (function init() {
        term.setLogLevel(term.getLogLevel());
    })();

    return term;
}